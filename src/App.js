import React, { useEffect } from "react";
import './App.css';
import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes
} from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import { GlobalAppBar } from './components/navigation/GlobalAppBar/GlobalAppBar';
import NotFound from './components/navigation/NotFound';
import { EssNameView } from "./views/ess-name/EssNameView";
import { HomeView } from "./views/home/HomeView";
import { SystemStructureView } from "./views/system-structure/SystemStructureView";
import { DeviceStructureView } from "./views/device-structure/DeviceStructureView";
import { ThemeProvider } from "@mui/material/styles"
import { theme } from "./style/Theme";

function App() {
  useEffect(() => {
    document.title = "Naming";
  }, []);

  return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Router>
          <GlobalAppBar>
            <Routes>
              <Route path="/" element={ <Navigate to="/home"/> } exact />
              <Route path="/home" element={ <HomeView/> } exact />
              <Route path="/ess-name" element={ <EssNameView/> } exact />
              <Route path="/system-structure" element={ <SystemStructureView/> } exact />
              <Route path="/device-structure" element={ <DeviceStructureView/> } exact />
              <Route path="*" element={ <NotFound/> }/>
            </Routes>
          </GlobalAppBar>
        </Router>
      </ThemeProvider>
  );
}

export default App;
