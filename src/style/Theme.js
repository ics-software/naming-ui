import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  drawer: {
    widthOpen: 240
  },
  palette: {
    primary: {
      main: "#0099dc",
    },
    secondary: {
      main: "#0d598e",
    },
  },
});