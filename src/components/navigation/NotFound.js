import React from 'react';
import {
  Typography,
  Box,
  Button
} from "@mui/material";
import { RootContainer } from "../../components/common/container/RootContainer";
import { RootPaper } from '../../components/common/container/RootPaper';
import { useNavigate } from 'react-router-dom';

export default function NotFound({message}) {
  const navigate = useNavigate();
  const goHome = () =>{ 
    navigate("/");
  }

  return (
    <RootContainer>
      <RootPaper>
        <Box display="flex" justifyContent="center">
          <Typography variant="h3">404</Typography>
        </Box>
        <Box display="flex" justifyContent="center" paddingTop={1} paddingBottom={3}>
          <Typography variant="h4">{message ?? " Page not found"}</Typography>
        </Box>
        <Box display="flex" justifyContent="center">
          <Button variant="contained" color="secondary" onClick={goHome} >Return to Home</Button>
        </Box>
      </RootPaper>
    </RootContainer>
  );
}