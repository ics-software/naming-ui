import * as React from 'react';
import { useState, useEffect, createContext, useContext } from "react";
import { styled, useTheme } from '@mui/material/styles';
import MuiAppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { Link } from "react-router-dom";
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { MenuDrawer } from "../../menu/MenuDrawer";

const AppBar = styled(MuiAppBar)(({ theme }) => ({
  zIndex: theme.zIndex.drawer + 1
  })
);

const defaultTitle = "Naming"

export const GlobalAppBarContext = createContext({
  title: defaultTitle,
  setTitle: () => { console.log("default GlobalAppBarContext.setTitle does nothing.") }
});

export default function ButtonAppBar({toggleDrawer, title}) {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <IconButton edge="start" onClick={toggleDrawer} color="inherit" aria-label="menu">
              <MenuIcon />
            </IconButton>
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            {title}
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

function MenuListItem({ url, text, toggleDrawer }) {
  const currentUrl = `${window.location}`;

  return (
      <ListItem button component={Link} to={url} onClick={toggleDrawer} selected={currentUrl.endsWith(url)}>
        <ListItemText primary={text} primaryTypographyProps={{ variant: "h5" }} />
      </ListItem>
  );
}

function MenuListItems({ menuItems, toggleDrawer }) {
  return (
    <>
      {menuItems.map(({ text, url }) => (
        <MenuListItem key={text} url={url} text={text} toggleDrawer={toggleDrawer}/>
      ))}
    </>
  );
}

export function GlobalAppBar({ children }) {
  const theme = useTheme();
  const [title, setTitle] = useState(defaultTitle);
  const [value] = useState({ setTitle });
  const [drawerOpen, setDrawerOpen] = useState(false);
  
  const toggleDrawer = () => {
    setDrawerOpen(!drawerOpen);
  }

  const makeLink = (text, url) => ({ text, url });
  const menuItemsAll = [
    makeLink("Home", "/home"),
    makeLink("ESS Name", "/ess-name"),
    makeLink("System structure", "/system-structure"),
    makeLink("Device structure", "/device-structure")
  ]

  return (
    <>
      <ButtonAppBar toggleDrawer={toggleDrawer} title={title} />
      <MenuDrawer open={drawerOpen} toggleDrawer={toggleDrawer}>
        <MenuListItems menuItems={menuItemsAll} toggleDrawer={toggleDrawer} />
      </MenuDrawer>
      <div>
        <GlobalAppBarContext.Provider value={value}>
          <div style={{ ...theme.mixins.toolbar }} />
            {children}
        </GlobalAppBarContext.Provider>
      </div>
    </>
  );
}

export function useGlobalAppBar(initTitle) {
  const [title, setTitle] = useState(initTitle);
  const appBar = useContext(GlobalAppBarContext);
  useEffect(() => {
    appBar.setTitle(defaultTitle + ' / ' + title );
  }, [appBar, title]);
  return { setTitle };
}
