import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import Container from '@mui/material/Container';

export function MenuDrawer({ open, toggleDrawer, children }) {
    const theme = useTheme();
  
    return (
      <>
        <SwipeableDrawer 
          anchor='left'
          open={open}
          onClose={toggleDrawer}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <Container disableGutters="true">
            <div style={{ ...theme.mixins.toolbar }} />
            <div style={{ paddingTop: theme.spacing(2), paddingBottom: theme.spacing(2) }} >
              {children}
            </div>
          </Container>
        </SwipeableDrawer>
      </>
    );
}