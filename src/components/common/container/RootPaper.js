import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';

export const RootPaper = styled(Paper)(({ theme }) => ({
  paddingLeft: theme.spacing(4),
  paddingRight: theme.spacing(4)
  })
);
