import { styled } from '@mui/material/styles';
import Container from '@mui/material/Container';

export const RootContainer = styled(Container)({
    paddingTop: "20px",
    paddingBottom: "200px",
});