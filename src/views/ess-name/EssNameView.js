import * as React from 'react';
import { useGlobalAppBar } from "../../components/navigation/GlobalAppBar/GlobalAppBar";
import { RootContainer } from "../../components/common/container/RootContainer";
import { RootPaper } from '../../components/common/container/RootPaper';

export function EssNameView() {
  useGlobalAppBar("ESS Names");

  return(
    <RootContainer>
      <RootPaper>
        <h2>Names</h2>
      </RootPaper>
    </RootContainer>
  )
}