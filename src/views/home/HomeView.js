import * as React from 'react';
import { useGlobalAppBar } from "../../components/navigation/GlobalAppBar/GlobalAppBar";
import { RootContainer } from "../../components/common/container/RootContainer";
import { RootPaper } from '../../components/common/container/RootPaper';


export function HomeView() {
  useGlobalAppBar("Home");

  return(
    <RootContainer>
      <RootPaper>
        <h2>Home</h2>
      </RootPaper>
    </RootContainer>
  )
}