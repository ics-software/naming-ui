import * as React from 'react';
import { useGlobalAppBar } from "../../components/navigation/GlobalAppBar/GlobalAppBar";
import { RootContainer } from "../../components/common/container/RootContainer";
import { RootPaper } from '../../components/common/container/RootPaper';


export function DeviceStructureView() {
  useGlobalAppBar("Device Structure");

  return(
    <RootContainer>
      <RootPaper>
        <h2>Device structure</h2>
      </RootPaper>
    </RootContainer>
  )
}