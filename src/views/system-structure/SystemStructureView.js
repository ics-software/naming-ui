import * as React from 'react';
import { useGlobalAppBar } from "../../components/navigation/GlobalAppBar/GlobalAppBar";
import { RootContainer } from "../../components/common/container/RootContainer";
import { RootPaper } from '../../components/common/container/RootPaper';


export function SystemStructureView() {
  useGlobalAppBar("System Structure");

  return(
    <RootContainer>
      <RootPaper>
        <h2>System structure</h2>
      </RootPaper>
    </RootContainer>
  )
}